package com.mobile.ict.cart;

import org.json.JSONException;
import org.json.JSONObject;


public class OrderRow {

    private String name;
    private double unitPrice;
    private double total;
    private double quantity;

    public double getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(double stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    private double stockQuantity;

    public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public OrderRow(String name, double unitPrice, double total,double stockQuantity) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = total/unitPrice;
        this.stockQuantity=stockQuantity;
    }

    public OrderRow(String name, double unitPrice, double total) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.total = total;
        this.quantity = total/unitPrice;

    }
    
    public OrderRow(JSONObject orderItem) throws JSONException {
        this.name = orderItem.getString("name");
        this.unitPrice = Double.parseDouble(orderItem.getString("rate"));
        this.quantity = Double.parseDouble(orderItem.getString("quantiity"));
        this.total = this.quantity * this.unitPrice;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
