package com.mobile.ict.cart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


@SuppressLint("NewApi")
public class NewOrderActivity extends Activity implements OnClickListener {

    Button b1,b2;
    EditText quantity;
    ProgressDialog pd;
    TextView heading;
    double grandTotal;
    Bundle bundle;
    Map <String,String> itemsMap = new HashMap<String, String>();
    Handler handler ;
    ListView list;
    OrderRowAdapter adapter;
    ArrayList<ItemRow> itemss;
    Dialog dialog ;
    ArrayList<OrderRow> items;
    String memberNumber;
    SharedPreferences sharedPref;
    ProductCategory productCategory;
    ArrayList<ProductCategory> groups ;
    OrderRow orderRow;
    ExpandableListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);

        heading=(TextView)findViewById(R.id.label_choose_order);
        b1=(Button)findViewById(com.mobile.ict.cart.R.id.button_proceed_confirm);
        b2=(Button) findViewById(com.mobile.ict.cart.R.id.button_draft);
        new LoginVerification(getApplicationContext(), itemsMap).execute(sharedPref.getString("orgAbbr",""));

        heading.setTypeface(tf1);
        b1.setTypeface(tf1);
        b2.setTypeface(tf1);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        bundle = new Bundle();
        listView = (ExpandableListView) findViewById(R.id.new_order_item_list);

        groups = new ArrayList<>();

        adapter = new OrderRowAdapter(this, groups,bundle);
        listView.setAdapter(adapter);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String imgSett = prefs.getString("groupId", "");


        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                pd.dismiss();
                if(getIntent().getSerializableExtra("itemsMap") != null){
                    itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
                    bundle = getIntent().getExtras();
                }
                adapter = new OrderRowAdapter(NewOrderActivity.this, groups,bundle);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

        };

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        MenuItem item_refresh = menu.findItem(R.id.action_refresh);

        MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

        item_editNumber.setVisible(false);
        item_editNumber.setEnabled(false);

        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);

        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case com.mobile.ict.cart.R.id.action_home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onClick(View view) {

        itemss = new ArrayList<ItemRow>();
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        grandTotal=0.0;
        for(Map.Entry<String, String> entry : sortList.entrySet()){
            double quantity = bundle.getDouble(entry.getKey(),0);
            if(quantity > 0) {
                double total = quantity * Double.parseDouble(entry.getValue());
                grandTotal += total;
                ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                itemss.add(item);

            }
        }

        if(view == b1){


            if(itemss.isEmpty())
            {

                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,Toast.LENGTH_LONG).show();

            }
            else
            {
                Intent i = new Intent(this,ConfirmOrderActivity.class);
                bundle.putSerializable("itemsMap", (Serializable) itemsMap);
                i.putExtras(bundle);
                startActivity(i);
                finishNewOrderActivity();

            }

        }





        if(view == b2){

            if(itemss.isEmpty())
            {

                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,Toast.LENGTH_LONG).show();

            }

            else
            {
                Intent i = new Intent(this, OrderDraftActivity.class);
                bundle.putSerializable("itemsMap", (Serializable) itemsMap);
                i.putExtras(bundle);
                startActivity(i);

            }



        }
    }





    @SuppressLint("NewApi")
    public class LoginVerification extends AsyncTask<String, String, String> {


        public LoginVerification(Context context, Map<String, String> map) {
            itemsMap = map;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(NewOrderActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String url = "http://ruralict.cse.iitb.ac.in/RuralIvrs/api/products/search/byType/map?orgabbr=" + params[0];


            JsonParser jParser = new JsonParser();

            String response = jParser.getJSONFromUrl(url, null, "GET", true, sharedPref.getString("emailid", null), sharedPref.getString("password", null));


           return response;

        }

        @Override
        protected void onPostExecute(String response)
        {

            try {
                JSONObject object = new JSONObject(response);


               // System.out.println("response-----------------"+response);

                itemsMap = JsonParser.categoryMap(object);



                 groups = new ArrayList<>();

                Iterator<String> keysItr = object.keys();
                while(keysItr.hasNext())
                {
                    String key = keysItr.next();

                    productCategory= new ProductCategory(key);
                    Object category =object.get(key);
                    if(category instanceof JSONArray)
                    {
                        for(int i=0; i<((JSONArray)category).length();i++)
                        {

                           // orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0,Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")));

                            orderRow=new OrderRow(((JSONArray)category).getJSONObject(i).getString("name"),Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate")),0.0);

                          //  System.out.println(((JSONArray)category).getJSONObject(i).getString("name")+"-------------"+Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("unitRate"))+"-------"+Double.parseDouble(((JSONArray)category).getJSONObject(i).getString("quantity")));

                            productCategory.productItems.add(orderRow);
                        }

                    }

                    groups.add(productCategory);

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                pd.dismiss();

                AlertDialog.Builder builder = new AlertDialog.Builder(NewOrderActivity.this);

                builder.setTitle(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();

                            }
                        })

                .setCancelable(false);

                dialog=builder.create();
                dialog.show();

            }

            Message msg = Message.obtain();
            handler.sendMessage(msg);


        }

    }



    public void finishNewOrderActivity() {
        NewOrderActivity.this.finish();
    }

}
