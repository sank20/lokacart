package com.mobile.ict.cart;

import android.app.Fragment;
import android.os.Bundle;

import org.json.JSONObject;

import java.util.ArrayList;

public class RetainedFragment extends Fragment {

    private int pos,id,alertDialogId,signUpOtpDialogId,forgotOtpDialogId,feedBackDialogId,optionIndex;

    String otherReasonText,mobileNumber;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtherReasonText() {
        return otherReasonText;

    }

    public void setOtherReasonText(String otherReasonText) {
        this.otherReasonText = otherReasonText;
    }

    public ArrayList<ArrayList<String[]>> getDraftorderlistitem() {
        return draftorderlistitem;
    }

    public void setDraftorderlistitem(ArrayList<ArrayList<String[]>> draftorderlistitem) {
        this.draftorderlistitem = draftorderlistitem;
    }

    public int getSignUpOtpDialogId() {
        return signUpOtpDialogId;
    }

    public int getOptionIndex() {
        return optionIndex;
    }

    public void setOptionIndex(int optionIndex) {
        this.optionIndex = optionIndex;
    }

    public int getFeedBackDialogId() {
        return feedBackDialogId;
    }

    public void setFeedBackDialogId(int feedBackDialogId) {
        this.feedBackDialogId = feedBackDialogId;
    }

    public void setSignUpOtpDialogId(int signUpOtpDialogId) {
        this.signUpOtpDialogId = signUpOtpDialogId;
    }

    public int getForgotOtpDialogId() {
        return forgotOtpDialogId;
    }

    public void setForgotOtpDialogId(int forgotOtpDialogId) {
        this.forgotOtpDialogId = forgotOtpDialogId;
    }

    private boolean status,groupDialogFlag;
    private ArrayList<JSONObject> list;
    private ArrayList<ArrayList<String[]>> draftorderlistitem;


    public int getAlertDialogId() {
        return alertDialogId;
    }

    public void setAlertDialogId(int alertDialogId) {
        this.alertDialogId = alertDialogId;
    }

    private ArrayList<DraftOrder> draftlist;

    public ArrayList<DraftOrder> getDraftlist() {
        return draftlist;
    }

    public void setDraftlist(ArrayList<DraftOrder> draftlist) {
        this.draftlist = draftlist;
    }

    public ArrayList getList() {
        return list;
    }

    public void setList(ArrayList<JSONObject> list) {
        this.list = list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getStatus() {
        return status;
    }

    public void setdialogId(int id) {
        this.id = id;
    }

    public int getdialogId() {
        return id;
    }


    public void setGroupDialogFlag(boolean groupDialogFlag) {
        this.groupDialogFlag = groupDialogFlag;
    }

    public boolean getGroupDialogFlag() {
        return groupDialogFlag;
    }
}
