package com.mobile.ict.cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CancelOrderItemAdapter extends ArrayAdapter<CancelOrder> {

    private List<CancelOrder> orderList = new ArrayList<>();
    private ProcessOrderAsyncTaskListener callback;
    CancelOrderViewHolder viewHolder;
    Context context;
    SharedPreferences sharedPref;
    Bundle b;

    public CancelOrderItemAdapter(Context context, int resource, List<CancelOrder> orderList) {
        super(context, resource, orderList);

        this.orderList = orderList;
        this.context = context;
        this.callback = (ProcessOrderAsyncTaskListener)context;
    }

    static class CancelOrderViewHolder {
        TextView orderId;
        Button cancel;
        TextView timeStamp;
    }

    @Override
    public void add(CancelOrder object) {
        orderList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public CancelOrder getItem(int index) {
        return this.orderList.get(index);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_cancel_order_item, parent, false);
            viewHolder = new CancelOrderViewHolder();
            viewHolder.orderId = (TextView) row.findViewById(R.id.cancel_order_orderId);
            viewHolder.timeStamp = (TextView) row.findViewById(R.id.cancel_order_timeStamp);
            viewHolder.cancel = (Button) row.findViewById(R.id.cancel_order_btn);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CancelOrderViewHolder)row.getTag();
        }

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        final CancelOrder order = getItem(position);
        viewHolder.orderId.setText(order.getOrder_id() + "");

        Date time = null;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(order.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);

        viewHolder.timeStamp.setText(date);

        viewHolder.cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                callback.onCancelOrder(sharedPref.getString("mobilenumber",""),order.getOrder_id());

            }

        });

        return row;
    }


}
