package com.mobile.ict.cart;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OrderDraftActivity extends Activity implements View.OnClickListener{

    Button b1,b2;
    TextView heading;
    double grandTotal;
    ArrayList<ItemRow> items,itemss;
    String memberNumber,orgAbbr,orgId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_order);

        ListView list = (ListView) findViewById(R.id.confirm_order_item_list);
        List<ItemRow> item_list = getItems();
        ItemRowAdapter adapter = new ItemRowAdapter(this,R.layout.view_order_row, item_list);
        list.setAdapter(adapter);
        TextView totalBill = (TextView) findViewById(R.id.confirm_order_total_bill);
       // totalBill.setText(grandTotal+"");
        totalBill.setText(Double.parseDouble(String.format("%.2f",grandTotal))+"");

        b1=(Button)findViewById(R.id.button_modify_order);
        b2=(Button) findViewById(R.id.button1);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
        heading=(TextView)findViewById(R.id.tvOrder2);
        heading.setTypeface(tf1);
        b1.setTypeface(tf1);
        b2.setTypeface(tf1);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);
        MenuItem item_refresh = menu.findItem(R.id.action_refresh);
        MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

        item_editNumber.setVisible(false);
        item_editNumber.setEnabled(false);
        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);
        item_home.setVisible(false);
        item_home.setEnabled(false);

        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public List<ItemRow> getItems() {

        items = new ArrayList<ItemRow>();

        

        Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        grandTotal=0.0;
        for(Map.Entry<String, String> entry : sortList.entrySet()){
            double quantity = getIntent().getDoubleExtra(entry.getKey(),0);
            if(quantity > 0) {
                double total = quantity *Double.parseDouble(entry.getValue());
                grandTotal += Double.parseDouble(String.format("%.2f",total));
              //  String str = String.format("%.2f",total);
              //  grandTotal += Double.parseDouble(str);
               // grandTotal += total;
               // ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                 ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), Double.parseDouble(String.format("%.2f",total)));

                items.add(item);
            }
        }


        return items;
    }

    @Override
    public void onClick(View view) {
        if(view==b1){
            Intent i = new Intent(this,NewOrderActivity.class);
            i.putExtras(getIntent().getExtras());
            startActivity(i);
        }
        if(view==b2){


            insertItems();
            Intent i = new Intent(this,DashboardActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }






      
    }



    public  void  insertItems()

    {
        OrdersDraftDBHelper ordersDb = new OrdersDraftDBHelper(getApplicationContext());
        itemss = new ArrayList<ItemRow>();

        Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
        Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
        grandTotal=0.0;
        for(Map.Entry<String, String> entry : sortList.entrySet()){
            double quantity = getIntent().getDoubleExtra(entry.getKey(),0);
            if(quantity > 0) {
                double total = quantity *Double.parseDouble(entry.getValue());
                String str = String.format("%.2f",total);
                grandTotal += Double.parseDouble(str);
               // grandTotal += total;
               // ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
                ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), Double.parseDouble(str));

                itemss.add(item);
            }
        }


        if(itemss.isEmpty())
        {
            Toast.makeText(getApplicationContext(), R.string.label_toast_Please_select_at_least_one_product, Toast.LENGTH_LONG).show();
        }

        else
        {

            try {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                orgAbbr=sharedPref.getString("orgAbbr","");
                orgId=sharedPref.getString("orgIdSelected","");
                memberNumber=sharedPref.getString("mobilenumber","");


                int draftIdCount = ordersDb.selectMaxDraftOrderID(orgAbbr,orgId,memberNumber);
                draftIdCount++;
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm aa");

                Date now = new Date();

                String strDate = sdfDate.format(now);
                String strTime = sdfTime.format(now);

                String timestamp= strDate+","+strTime;



                for (ItemRow item : itemss) {


                    ordersDb.insertDraftOrder(orgId,orgAbbr,draftIdCount,Double.parseDouble(item.getUnitPrice().toString()),Double.parseDouble(item.getQuantity().toString()) ,Double.parseDouble(item.getTotal().toString()) ,item.getName().toString(),memberNumber,timestamp);

                }





            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}

