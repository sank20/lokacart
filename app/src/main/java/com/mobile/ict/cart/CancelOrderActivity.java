package com.mobile.ict.cart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class CancelOrderActivity extends Activity implements OnClickListener,ProcessOrderAsyncTaskListener {

    TextView heading;
    Bundle bundle;
    Map <String,String> itemsMap = new TreeMap<String, String>();
    EditText otherReason;
    ListView orderList,orderListItem;
    CancelOrderItemAdapter adapter;
    CancelOrderListAdapter listadapter;
    ArrayList<JSONObject> orderObjects;
    ArrayList<com.mobile.ict.cart.CancelOrder> orders;
    String memberNumber,abbr;
    Dialog dialog,feedbackDialog ;
    Window window;
    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    Point size;
    WindowManager windowManager;
    Display display;
    int width,height,dialogid,alertdialogid,feedbackdialogid,optionIndex=-1;
    ArrayList<com.mobile.ict.cart.CancelOrder> saved_order_list;
    private RetainedFragment dataFragment;
    int pos;
    Handler handler ;
    SharedPreferences sharedPref;
    String options="",comments,otherReasonText="";

    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_orders);



        String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
        Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
        heading=(TextView)findViewById(R.id.cancel_order_label);
        heading.setTypeface(tf1);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        memberNumber=sharedPref.getString("mobilenumber","");
        abbr=sharedPref.getString("orgAbbr","");

        bundle = new Bundle();

        orderList = (ListView) findViewById(R.id.cancel_orders_list);

        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("CancelOrderActivity");


        if(dataFragment==null)
        {

            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "CancelOrderActivity").commit();
            dialogid=-1;
            alertdialogid=-1;
            feedbackdialogid=-1;
            dataFragment.setdialogId(dialogid);
            dataFragment.setAlertDialogId(alertdialogid);
            dataFragment.setFeedBackDialogId(feedbackdialogid);

            new FetchOrders(CancelOrderActivity.this, itemsMap).execute("null");




        }
        else
        {




            if(dataFragment.getAlertDialogId()==1)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(CancelOrderActivity.this);

                builder.setTitle(R.string.label_alertdialog_No_Cancel_Orders_Found)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();

                            }
                        });


                dialog = builder.show();

            }


            else
            {
                orderObjects= dataFragment.getList();
                saved_order_list = getOrders();

                adapter = new CancelOrderItemAdapter(CancelOrderActivity.this,R.layout.activity_cancel_order_item,saved_order_list);
                orderList.setItemsCanFocus(false);
                orderList.setAdapter(adapter);

                if(dataFragment.getdialogId() == 1)
            {
                openDialog(dataFragment.getPos());
                if(dataFragment.getFeedBackDialogId() == 1)
                {


                    optionIndex=dataFragment.getOptionIndex();

                    otherReasonText=dataFragment.getOtherReasonText();

                    openFeedBackDialog();

                }



            }


            }






        }





        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);

                saved_order_list = getOrders();

                adapter = new CancelOrderItemAdapter(CancelOrderActivity.this,R.layout.activity_cancel_order_item,saved_order_list);
                orderList.setItemsCanFocus(false);
                orderList.setAdapter(adapter);
              //  unlockScreenOrientation();

            }

        };








        orderList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                dialogid = 1;
                pos = position;
                openDialog(pos);


            }
        });




    }




   ;


    public void openDialog(int position)
    {

        dialog = new Dialog(CancelOrderActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.activity_cancel_order_list, (ViewGroup) findViewById(R.id.cancel_order), false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(Layout);

        com.mobile.ict.cart.CancelOrder order = orders.get(position);
        orderListItem =  (ListView)Layout.findViewById(R.id.cancel_order_list);




        listadapter = new CancelOrderListAdapter(CancelOrderActivity.this,order.getItemsList(pos));
        orderListItem.setAdapter(listadapter);

        TextView totalBill = (TextView)Layout.findViewById(R.id.cancel_order_total_bill);
        totalBill.setText(""+order.totalBill);


        window =dialog.getWindow();

        size = new Point();
        windowManager = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
            windowManager.getDefaultDisplay().getSize(size);

            width = size.x;
            height = size.y;
        }else{
            display = windowManager.getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();
        }



        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            lp.width=(int)(width/1.125);
            lp.height = (int)(height/1.5);


        } else {
            lp.width=width/2;
            lp.height = (int)(height/1.25);


        }



        lp.gravity = Gravity.CENTER;

        window.setAttributes(lp);

        dialog.setCancelable(false);
        dialog.show();



    }







    public void openFeedBackDialog()
    {
        dialogid=1;
        feedbackDialog = new Dialog(CancelOrderActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        final View Layout = inflater.inflate(R.layout.cancel_order_feedback, (ViewGroup) findViewById(R.id.cancel_order_feedback), false);
        feedbackDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedbackDialog.setContentView(Layout);
        final ListView lv = (ListView)Layout.findViewById(R.id.lv1);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        otherReason =(EditText)Layout.findViewById(R.id.comments);
        otherReason.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if(!s.equals(""))
                otherReasonText=s.toString();
                else
                    otherReasonText="";
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        final ArrayList<HashMap<String, Object>> m_data = new ArrayList<HashMap<String, Object>>();

        HashMap<String, Object> map1 = new HashMap<String, Object>();
        map1.put("option",getResources().getString(R.string.label_cancelorder_feedback_form_option1));
        m_data.add(map1);

        HashMap<String, Object> map2 = new HashMap<String, Object>();
        map2.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option2));// no small text of this item!
        m_data.add(map2);

        HashMap<String, Object> map3 = new HashMap<String, Object>();
        map3.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option3));
        m_data.add(map3);

        HashMap<String, Object> map4 = new HashMap<String, Object>();
        map4.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option4));
        m_data.add(map4);

        HashMap<String, Object> map5 = new HashMap<String, Object>();
        map5.put("option", getResources().getString(R.string.label_cancelorder_feedback_form_option5));
        m_data.add(map5);

        for (HashMap<String, Object> m :m_data)
            m.put("checked", false);

        if(dataFragment.getFeedBackDialogId()==1 && optionIndex!=-1)
        {
            m_data.get(optionIndex).put("checked", true);
            options=m_data.get(optionIndex).get("option").toString();
            if(m_data.get(optionIndex).get("option").equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5)))
            {
                otherReason.setVisibility(View.VISIBLE);

                if(!otherReasonText.equals(""))
                    otherReason.setText(otherReasonText);

            }
        }

        final SimpleAdapter adapter = new SimpleAdapter(CancelOrderActivity.this,
                m_data,
                R.layout.cancelorder_feedback_item,
                new String[] {"option","checked"},
                new int[] {R.id.options, R.id.rb_choice});

        adapter.setViewBinder(new SimpleAdapter.ViewBinder()
        {
            public boolean setViewValue(View view, Object data, String textRepresentation)
            {

                view.setVisibility(View.VISIBLE);
                return false;
            }

        });


        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {
                RadioButton rb = (RadioButton) v.findViewById(R.id.rb_choice);

                if(m_data.get(arg2).get("option").equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5)))
                {
                    otherReason.setVisibility(View.VISIBLE);
                }
                else
                {
                    if(otherReason.isShown())
                    {
                        otherReason.setVisibility(View.GONE);
                        otherReason.setError(null);

                    }

                }

                options=m_data.get(arg2).get("option").toString();

                optionIndex = arg2;

                if (!rb.isChecked())
                {
                    for (HashMap<String, Object> m :m_data)
                        m.put("checked", false);
                    m_data.get(arg2).put("checked", true);
                    adapter.notifyDataSetChanged();
                }
            }
        });





        window =feedbackDialog.getWindow();

        size = new Point();
        windowManager = getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2){
            windowManager.getDefaultDisplay().getSize(size);

            width = size.x;
            height = size.y;
        }else{
            display = windowManager.getDefaultDisplay();
            width = display.getWidth();
            height = display.getHeight();
        }



        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            lp.width=(int)(width/1.125);
            lp.height = (int)(height/1.5);


        } else {
            lp.width=width/2;
            lp.height = (int)(height/1.25);


        }



        lp.gravity = Gravity.CENTER;

        window.setAttributes(lp);

        feedbackDialog.setCancelable(false);
        feedbackDialog.show();

    }



    public void feedbackOk(View v)
    {

        if(options.equals(""))
        {
            Toast.makeText(CancelOrderActivity.this,R.string.label_toast_cancel_order_feedback_form, Toast.LENGTH_SHORT).show();

        }
        else
        {
            if(options.equals(getResources().getString(R.string.label_cancelorder_feedback_form_option5)))
            {
                if(!otherReason.getText().toString().equals(""))
                {

                    comments=otherReason.getText().toString().replaceAll("\\n", " ");
                    otherReasonText=comments;
                    feedbackdialogid=-1;
                    feedbackDialog.dismiss();
                    new CancelOrder(CancelOrderActivity.this).execute(String.valueOf(saved_order_list.get(pos).getOrder_id()));



                }
                else
                {
                    Validation.hasText(this,otherReason);

                }
            }
            else
            {
                comments=options;
                feedbackdialogid=-1;
                feedbackDialog.dismiss();
                new CancelOrder(CancelOrderActivity.this).execute(String.valueOf(saved_order_list.get(pos).getOrder_id()));



            }
        }

        options="";


    }

    public void feedbackClose(View v)
    {
        feedbackdialogid=-1;
        optionIndex=-1;
        otherReasonText="";
        dataFragment.setOtherReasonText(otherReasonText);
        dataFragment.setOptionIndex(optionIndex);
        dialogid=1;
        feedbackDialog.dismiss();

    }




    @Override
    protected void onStart() {
        super.onStart();


    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();

        dataFragment.setList(orderObjects);


        if(alertdialogid==1)
        {

            dataFragment.setAlertDialogId(alertdialogid);
        }
        else
        {
                      if(dialogid==1) {

                dataFragment.setdialogId(dialogid);
                dataFragment.setPos(pos);

                if(feedbackdialogid==1)
                {

                    dataFragment.setFeedBackDialogId(feedbackdialogid);

                }
                else
                {

                    if(feedbackdialogid==-1)
                        dataFragment.setFeedBackDialogId(feedbackdialogid);
                }
                dataFragment.setOptionIndex(optionIndex);
                dataFragment.setOtherReasonText(otherReasonText);

            }
            else
            {
                if(dialogid==-1)
                    dataFragment.setdialogId(dialogid);


            }
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (dialog != null) dialog.dismiss();
        if (feedbackDialog != null) feedbackDialog.dismiss();


    }


    public void close(View v)
    {
        dialogid=-1;
        feedbackdialogid=-1;
        dialog.dismiss();

    }



    public void cancelOrder(View v)
    {
        dialogid=1;
        feedbackdialogid=1;
        openFeedBackDialog();

    }



    public void modifyOrder(View v)
    {
        dialogid=-1;
        feedbackdialogid=-1;

        dialog.dismiss();
        com.mobile.ict.cart.CancelOrder order = orders.get(pos);

        itemsMap.clear();
        ArrayList<String[]> list = order.getItemsList(pos);

        for(int j=0;j<list.size();j++)
        {

            itemsMap.put(list.get(j)[0],list.get(j)[1]+"");

        }

        Intent i=new Intent(getApplicationContext(),ModifyOrderActivity.class);
        i.putExtra("itemsMap", (Serializable) itemsMap);
        i.putExtra("orderId",orders.get(pos).getOrder_id());
        for(int j=0;j<list.size();j++)
        {
            i.putExtra(list.get(j)[0],Double.parseDouble(list.get(j)[2]));
        }
        startActivity(i);
    }





    public ArrayList<com.mobile.ict.cart.CancelOrder> getOrders() {

        orders = new ArrayList<>();

        int count=0;


        if(orderObjects.size()!=0)

        {
            for (JSONObject entry : orderObjects) {
                com.mobile.ict.cart.CancelOrder order = new com.mobile.ict.cart.CancelOrder(entry, memberNumber, count);
                orders.add(order);
                count++;
            }
        }
        else
        {

            alertdialogid=1;






            AlertDialog.Builder builder = new AlertDialog.Builder(CancelOrderActivity.this);

            builder.setTitle(R.string.label_alertdialog_No_Cancel_Orders_Found)
                    .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            finish();

                        }
                    });


            dialog = builder.show();



        }

        return orders;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_order, menu);
        MenuItem item_help = menu.findItem(R.id.action_help);
        MenuItem item_logout = menu.findItem(R.id.action_logout);
        MenuItem item_home = menu.findItem(R.id.action_home);

        MenuItem item_refresh = menu.findItem(R.id.action_refresh);

        MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

        item_editNumber.setVisible(false);
        item_editNumber.setEnabled(false);

        item_refresh.setVisible(false);
        item_refresh.setEnabled(false);

        item_home.setVisible(false);
        item_home.setEnabled(false);
        item_help.setVisible(false);
        item_logout.setVisible(false);
        item_help.setEnabled(false);
        item_logout.setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case com.mobile.ict.cart.R.id.action_home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCancelOrder(String number, int orderid) {

        dialogid=1;
        openDialog(pos);
    }

    @SuppressLint("NewApi")
    public class FetchOrders extends AsyncTask<String,String, String> {

        Context context;


        public FetchOrders(Context context, Map<String, String> map){
            this.context=context;
            itemsMap = map;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // lockScreenOrientation();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


           String url = "http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/search/getOrdersForMember?format=binary&status=saved&abbr="+abbr+"&phonenumber=91"+memberNumber+"&projection=default";

            JsonParser jParser = new JsonParser();
            String response = jParser.getJSONFromUrl(url,null,"GET",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

            return response;
        }

        @Override
        protected void onPostExecute(String response) {

           pd.dismiss();

            if(response.equals("exception"))
            {
                Dialog dia;
                AlertDialog.Builder builder = new AlertDialog.Builder(CancelOrderActivity.this);

                builder.setTitle(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                finish();

                            }
                        })

                        .setCancelable(false);

                dia=builder.create();
                dia.show();
            }
            else
            {
                try {


                    JSONObject orderList = new JSONObject(response);
                    JSONArray ordersArray = orderList.getJSONObject("_embedded").getJSONArray("orders");
                    orderObjects=new ArrayList<JSONObject>();

                        for(int i=0;i<ordersArray.length();i++)
                        {
                            orderObjects.add((JSONObject)ordersArray.get(i));

                        }

                    Message msg = Message.obtain();
                    handler.sendMessage(msg);





                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                   // e.printStackTrace();

                    Dialog dia;
                    AlertDialog.Builder builder = new AlertDialog.Builder(CancelOrderActivity.this);

                    builder.setTitle(R.string.label_alertdialog_No_Cancel_Orders_Found)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    finish();

                                }
                            })

                            .setCancelable(false);

                    dia=builder.create();
                    dia.show();

                }




            }







        }



    }


    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }



    public class CancelOrder extends AsyncTask<String, String, String>{

        Context context;
        String val;

        public CancelOrder(Context context){
            this.context=context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

           // lockScreenOrientation();
            pd = new ProgressDialog(CancelOrderActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub


            String url=" http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/update/"+params[0];

                JsonParser jParser = new JsonParser();
                JSONObject obj = new JSONObject();
            try {
                obj.put("status","cancelled");
                obj.put("comments",comments);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String response = jParser.getJSONFromUrl(url,obj,"POST",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

                return response;


        }

        protected void onPostExecute(String response1) {

                pd.dismiss();



            if(response1.equals("exception"))
            {
                Toast.makeText(context,R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
            }
            else {
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(response1);
                    val=jsonObj.getString("Status");

                    if(val.equals("Success"))
                    {

                        Toast.makeText(context,R.string.label_toast_Your_order_has_been_cancelled_successfully, Toast.LENGTH_SHORT).show();
                    }

                    else

                    Toast.makeText(context,R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Intent i = new Intent(context, CancelOrderActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);


            }


           // unlockScreenOrientation();


        }

    }









}
