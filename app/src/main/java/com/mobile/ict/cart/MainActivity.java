package com.mobile.ict.cart;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity {

    Intent i;
    String memberNumber="",otp_checkA,email;
    EditText  numberA,otpA;

    MobileNumberVerification mv;
    Button otp_btn,confirm_btn,cancel_btn;

    OTPCountDownTimer countDownTimerA;
    String responseA;
    boolean login;

    EditText phoneNumber,password,otp,pass1,pass2,emailID;
    String number,otp_check,pwd,pwd1,pwd2,response,response2,response3,versionName;
    LoginVerification loginVerification;
    JSONObject resObj;
    int dialogid=-1,forgotOtpDialogId,signUpOtpDialogId,chkversiondialogid=-1;
    Button getOtp,Confirm,Cancel;
    GetOTPTask anp;
    AddPasswordTask addPasswordTask;
    long session;
    OTPCountDownTimer countDownTimer;
    CheckBox chkBox;

    SharedPreferences sharedPref;
    private RetainedFragment dataFragment;
    SharedPreferences.Editor editor;
    int dialogBox=0;
    float versionCode = 0.0f;

    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();

        if(connect())
        {

            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            }
            catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
            new CheckApkVersionTask(MainActivity.this).execute(versionName);

        }


      else
        {
            if(sharedPref.getInt("versionchk", -1)==-1 || sharedPref.getInt("versionchk", -1) == 0)
                initialize();
            else if(sharedPref.getInt("versionchk", -1)==1)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(R.string.label_alertdialog_app_update_msg)
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                initialize();
                            }
                        }).setCancelable(false);


                dialog = builder.show();



            }
            else
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(R.string.label_alertdialog_app_update_msg)
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        }).setCancelable(false);

                dialog = builder.show();
            }


        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_help) {
            Intent intent = new Intent(this,HelpActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case 0:
                setResult(0);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    if(chkversiondialogid==1) {

        dataFragment.setdialogId(dialogid);

        if (dialogid == 2)
        {
            dataFragment.setMobileNumber(number);


        }

    }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialog!=null && dialog.isShowing())dialog.dismiss();
       /* if(signUpOtpDialogId==1){
            countDownTimerA.cancel();
        }*/
        if(forgotOtpDialogId==2){
            countDownTimer.cancel();
        }


    }


    public void signUp(View v)
    {
        dialogid=1;

        if(connect())
        {

            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.authentication_box);
            dialog.setTitle(R.string.label_activity_main_authentication);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            numberA = (EditText) dialog.findViewById(R.id.number);
            emailID = (EditText) dialog.findViewById(R.id.usremail);

            otp = (EditText) dialog.findViewById(R.id.OTP);
            otp.setVisibility(View.GONE);

            otp_btn = (Button)dialog. findViewById(R.id.buttonOTP);

            confirm_btn = (Button)dialog.findViewById(R.id.buttonSubmit);
            confirm_btn.setVisibility(View.GONE);

            cancel_btn = (Button)dialog. findViewById(R.id.buttoncancel);

            otp_btn.setOnClickListener(new View.OnClickListener()


                                       {
                                           @Override
                                           public void onClick(View v) {

                                               if(!numberA.getText().toString().equals("")&&!emailID.getText().toString().equals(""))
                                               {

                                                   long i = Long.parseLong(numberA.getText().toString());
                                                   long length = (long)(Math.log10(i)+1);
                                                   if(length<10 || length > 10)
                                                      {
                                                       Toast.makeText(getApplicationContext(),R.string.label_toast_Enter_valid_mobile_number, Toast.LENGTH_LONG).show();
                                                       }
                                                   else{

                                                       if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailID.getText().toString()).matches())

                                                           Toast.makeText(getApplicationContext(),R.string.label_validation_email, Toast.LENGTH_LONG).show();

                                                       else
                                                       {
                                                           memberNumber=numberA.getText().toString();
                                                           email=emailID.getText().toString();

                                                           JSONObject obj = new JSONObject();

                                                           try {
                                                               obj.put("phonenumber","91"+memberNumber);
                                                               obj.put("email",email);
                                                           } catch (JSONException e) {
                                                               e.printStackTrace();
                                                           }

                                                           mv = new MobileNumberVerification(MainActivity.this);
                                                           mv.execute(obj);
                                                       }


                                                   }
                                               }
                                               else
                                               {
                                                   if(numberA.getText().toString().equals("") && emailID.getText().toString().equals("") )
                                                       Toast.makeText(getApplicationContext(), R.string.label_validation_email_mobile, Toast.LENGTH_LONG).show();
                                                   else
                                                   if(numberA.getText().toString().equals(""))
                                                       Toast.makeText(getApplicationContext(), R.string.label_toast_Please_enter_mobile_number, Toast.LENGTH_LONG).show();
                                                   else
                                                       Toast.makeText(getApplicationContext(), R.string.label_validation_email, Toast.LENGTH_LONG).show();

                                                   otp.setEnabled(false);

                                               }

                                           }

                                           // Perform button logic
                                       }
            );

            confirm_btn.setOnClickListener(new View.OnClickListener()


                                           {
                                               @Override
                                               public void onClick(View v) {

                                                   long session2 = System.currentTimeMillis();
                                                   System.out.println(responseA);

                                                   if(session2<session)
                                                   {
                                                       if(otp.getText().toString().equals(otp_checkA))
                                                       {
                                                           dialog.dismiss();
                                                           countDownTimerA.cancel();
                                                           signUpOtpDialogId=-1;
                                                           dialogid=-1;
                                                           i =  new Intent(MainActivity.this,SignUpActivity.class);
                                                           i.putExtra("json",responseA);
                                                           i.putExtra("mobilenumber",memberNumber);
                                                           i.putExtra("emailid",email);
                                                           startActivity(i);
                                                       }
                                                       else
                                                       {
                                                           otp.setHint(R.string.hint_activity_main_otp);
                                                           Toast.makeText(getApplicationContext(),R.string.label_toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();

                                                       }
                                                   }
                                                   else
                                                   {
                                                       otp.setText("");
                                                       otp.setHint(R.string.hint_activity_main_otp);
                                                       otp.setEnabled(false);
                                                       otp_btn.setEnabled(true);
                                                       otp_btn.setVisibility(View.VISIBLE);
                                                       cancel_btn.setEnabled(true);
                                                       cancel_btn.setVisibility(View.VISIBLE);
                                                       numberA.setEnabled(true);
                                                       emailID.setEnabled(true);

                                                       Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();

                                                   }


                                               }

                                               // Perform button logic
                                           }



            );
            cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialogid=-1;
                    dialog.dismiss();

                }
            });

            dialog.show();


        }
        else
        {
            Toast.makeText(this,R.string.label_toast_Please_check_WIFI_Mobile_data_connection,Toast.LENGTH_SHORT).show();
        }
    }


    public boolean connect(){
        String connection= "false";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){

            connection="true";
        }
        Boolean con = Boolean.valueOf(connection);
        return con;

    }




    public class MobileNumberVerification extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public MobileNumberVerification(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... loginobj) {


            String url ="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/otp";
            JsonParser jParser = new JsonParser();
            responseA = jParser.getJSONFromUrl(url,loginobj[0],"POST",false,null,null);
            return responseA;
        }





        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();


            if(response.equals("exception"))
            {


                myDialogFragment ob2=new myDialogFragment();


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");



            }
            else
            {



                try {
                    JSONObject obj = new JSONObject(response);

                    String otp_val = obj.getString("otp");
                    String text = obj.getString("text");

                    if(!otp_val.equals("null")&&!text.equals("null"))
                    {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_has_sent_to_your_mobile_sms,Toast.LENGTH_LONG).show();

                        otp.setVisibility(View.VISIBLE);
                        otp.setEnabled(true);
                        confirm_btn.setVisibility(View.VISIBLE);
                        confirm_btn.setEnabled(true);
                        numberA.setEnabled(false);
                        emailID.setEnabled(false);
                        otp_checkA = obj.getString("otp");
                        otp_btn.setVisibility(View.GONE);
                        cancel_btn.setVisibility(View.GONE);
                        signUpOtpDialogId=1;
                        session = System.currentTimeMillis();
                        countDownTimerA = new OTPCountDownTimer(900000, 1000);
                        countDownTimerA.start();
                        dialogBox=0;
                        session = session+900000;

                    }

                    else
                    {
                        otp.setEnabled(false);
                        if(otp_val.equals("null")&&text.equals("Email entered already exists."))
                        {
                            Toast.makeText(getApplicationContext(),R.string.label_validation_Email_already_exists, Toast.LENGTH_LONG).show();

                        }
                        else
                        {
                            if(otp_val.equals("null")&&text.equals("Phone number entered already exists."))
                            {
                                Toast.makeText(getApplicationContext(),R.string.label_validation_Mobile_Number_exists, Toast.LENGTH_LONG).show();

                            }
                               else
                            Toast.makeText(getApplicationContext(),R.string.label_toast_You_are_already_registered_user, Toast.LENGTH_LONG).show();

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }




        }
    }

    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                    .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                             dialog.dismiss();

                                }
                            }
                    );
            return builder.create();
        }


    }


    public class OTPCountDownTimer extends CountDownTimer
    {

        public OTPCountDownTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }

        @Override
        public void onFinish()
        {
            otp.setText("");
            otp.setHint(R.string.hint_activity_main_otp);
            otp.setEnabled(false);
            cancel_btn.setEnabled(true);
            cancel_btn.setVisibility(View.VISIBLE);
            otp_btn.setEnabled(true);
            otp_btn.setVisibility(View.VISIBLE);

            if(dialogBox==0)
            {
                numberA.setEnabled(true);
                emailID.setEnabled(true);
                numberA.setVisibility(View.VISIBLE);
                emailID.setVisibility(View.VISIBLE);

            }else if(dialogBox==1)
            {
                pass1.setText("");
                pass1.setHint(R.string.hint_activity_main_new_password);
                pass1.setEnabled(true);

                pass2.setText("");
                pass2.setHint(R.string.hint_activity_main_confirm_password);
                pass2.setEnabled(true);
                pass1.setVisibility(View.VISIBLE);
                pass2.setVisibility(View.VISIBLE);
            }

            confirm_btn.setEnabled(false);
            confirm_btn.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();

        }

        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }















    public void login(View v){


        number = phoneNumber.getText().toString();


        pwd = password.getText().toString();


        if(checkValidation("Login"))
        {
            if(connect()){
                JSONObject obj = new JSONObject();
                try {
                    obj.put("phonenumber","91"+number);
                    obj.put("password",pwd);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                loginVerification = new LoginVerification(MainActivity.this);
                loginVerification.execute(obj);
            }
            else{
                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

            }
        }





    }





    private boolean checkValidation(String msg) {
        boolean ret = true;

        if (!Validation.isMobileNumber(this,phoneNumber, true, "mobileno"))
        {
            phoneNumber.setText("");
            ret = false;
        }
        if(msg.equals("Login"))
        {
            if(!Validation.hasText(this,password))
            {
                password.setText("");
                ret = false;

            }



        }




        return ret;
    }





    public class LoginVerification extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public LoginVerification(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            String url1= "http://ruralict.cse.iitb.ac.in/RuralIvrs/app/login";
            JsonParser jParser = new JsonParser();
            response = jParser.getJSONFromUrl(url1,params[0],"POST",false,null,null);
            return response;

        }

        @Override
        protected void onPostExecute(String response) {

            pd.dismiss();

              System.out.println(response.toString());
            if (response.equals("exception")) {

                myDialogFragment ob2 = new myDialogFragment();
                android.app.FragmentManager fm = getFragmentManager();

                ob2.show(fm, "1");
            } else {

                try {
                    resObj = new JSONObject(response);
                } catch (JSONException e) {
                }

                try {
                    if (resObj.get("Authentication").toString().equals("success")) {




                            try {

                                String emailId = resObj.getString("email");


                                if(chkBox.isChecked())
                                editor.putBoolean("signchk",true);
                                else
                                editor.putBoolean("signchk",false);

                                editor.putString("emailid",emailId);
                                editor.putString("password",pwd);
                                editor.putString("mobilenumber",number);
                                editor.putString("json",resObj.toString());
                                editor.commit();
                                Intent i = new Intent(MainActivity.this, DashboardActivity.class);
                                startActivityForResult(i, 0);


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }




                    } else {

                        if(resObj.get("registered").toString().equals("false"))
                        {
                            Toast.makeText(getApplicationContext(),R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_LONG).show();
                            phoneNumber.setText("");
                            password.setText("");
                        }
                        else
                        {
                            password.setError(getResources().getString(R.string.label_validation_enter_correct_password));
                            password.setText("");

                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }


            }

        }
    }





    public void forgot(View view)   {

        if(dataFragment.getdialogId()==2)
        {
            phoneNumber.setText(number);


        }else
        {
            number = phoneNumber.getText().toString();

        }

        dialogid=2;
        if(checkValidation("Forgot"))
        {


            if(connect())
            {

                dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.new_password_box);
                dialog.setTitle(R.string.label_dialog_title_verification);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                otp = (EditText) dialog.findViewById(R.id.enterOTP);
                otp.setVisibility(View.GONE);

                pass1=(EditText) dialog.findViewById(R.id.Password1);
                pass1.setVisibility(View.GONE);

                pass2=(EditText) dialog.findViewById(R.id.Password2);
                pass2.setVisibility(View.GONE);

                confirm_btn = (Button)dialog. findViewById(R.id.buttonConfirm);
                confirm_btn.setVisibility(View.GONE);

                otp_btn = (Button)dialog. findViewById(R.id.getOTP);
                cancel_btn = (Button)dialog. findViewById(R.id.cancel);

                cancel_btn.setOnClickListener(new View.OnClickListener()


                                          {
                                              @Override
                                              public void onClick(View v) {
                                                  dialogid=-1;
                                                  dialog.dismiss();

                                              }

                                              // Perform button logic
                                          }
                );

                otp_btn.setOnClickListener(new View.OnClickListener()


                                          {
                                              @Override
                                              public void onClick(View v) {


                                                  JSONObject obj = new JSONObject();

                                                  try {
                                                      obj.put("phonenumber","91"+number);

                                                      System.out.println("------forgot password-----"+obj.toString());
                                                      anp = new GetOTPTask(MainActivity.this);
                                                      anp.execute(obj);
                                                  } catch (JSONException e) {
                                                      e.printStackTrace();
                                                  }



                                              }

                                              // Perform button logic
                                          }
                );

                confirm_btn.setOnClickListener(new View.OnClickListener()


                                           {
                                               @Override
                                               public void onClick(View v) {

                                                   long session2 = System.currentTimeMillis();
                                                   if(session2<session)
                                                   {
                                                       if(otp.getText().toString().equals(otp_check))
                                                       {
                                                           countDownTimer.cancel();
                                                           pwd1 = pass1.getText().toString();
                                                           pwd2 = pass2.getText().toString();

                                                           if(!pwd1.equals(""))
                                                           {
                                                               if(pwd1.equals(pwd2)){
                                                                   try{
                                                                       JSONObject addPasswordDB= new JSONObject();
                                                                       addPasswordDB.put("phonenumber","91"+number);
                                                                       addPasswordDB.put("password", pwd1);
                                                                       if(connect()){
                                                                           addPasswordTask = new AddPasswordTask(MainActivity.this);
                                                                           addPasswordTask.execute(addPasswordDB);
                                                                       }
                                                                       else{
                                                                           Toast.makeText(getApplicationContext(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                                                                       }

                                                                   }catch(JSONException e){
                                                                       e.printStackTrace();
                                                                   }

                                                               }
                                                               else {
                                                                   pass1.setText("");
                                                                   pass2.setText("");
                                                                   Toast.makeText(getApplicationContext(),R.string.label_toast_Passwords_do_not_match, Toast.LENGTH_LONG).show();

                                                               }

                                                           }
                                                           else
                                                           {
                                                               pass1.setText("");
                                                               pass2.setText("");
                                                               Toast.makeText(getApplicationContext(),R.string.label_toast_Please_enter_password, Toast.LENGTH_LONG).show();
                                                           }


                                                       }
                                                       else
                                                       {
                                                           otp.setHint(R.string.hint_activity_main_otp);
                                                           Toast.makeText(getApplicationContext(), R.string.label_toast_Please_enter_correct_OTP, Toast.LENGTH_LONG).show();

                                                       }
                                                   }
                                                   else
                                                   {
                                                       otp.setText("");
                                                       otp.setHint(R.string.hint_activity_main_otp);
                                                       otp.setEnabled(false);

                                                       pass1.setText("");
                                                       pass1.setHint(R.string.hint_activity_main_new_password);
                                                       pass1.setEnabled(false);

                                                       pass2.setText("");
                                                       pass2.setHint(R.string.hint_activity_main_confirm_password);
                                                       pass2.setEnabled(false);

                                                       otp_btn.setEnabled(true);
                                                       confirm_btn.setVisibility(View.GONE);
                                                       confirm_btn.setEnabled(false);
                                                       cancel_btn.setVisibility(View.VISIBLE);
                                                       otp_btn.setVisibility(View.VISIBLE);

                                                       Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_your_OTP_expires___Try_to_get_new_OTP, Toast.LENGTH_LONG).show();

                                                   }


                                               }

                                               // Perform button logic
                                           }
                );


                dialog.show();



            }
            else
            {
                Toast.makeText(this,R.string.label_toast_Please_check_WIFI_Mobile_data_connection,Toast.LENGTH_SHORT).show();
            }

        }



    }


    public class GetOTPTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public GetOTPTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url=" http://ruralict.cse.iitb.ac.in/RuralIvrs/app/forgotpassword";
            JsonParser jParser = new JsonParser();
            response2 = jParser.getJSONFromUrl(url,params[0],"POST",false,null,null);


            return response2;
        }





        @Override
        protected void onPostExecute(String response2) {

            pd.dismiss();


            if(response2.equals("exception"))
            {

                myDialogFragment ob2=new myDialogFragment();
                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");
            }
            else
            {



                try {
                    JSONObject obj = new JSONObject(response2);

                    otp_check = obj.getString("otp");

                    System.out.println("forgot otp------------"+otp_check.toString());

                    if(otp_check.equals("null")){
                        Toast.makeText(getApplicationContext(), R.string.label_toast_You_are_not_a_registered_member, Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_OTP_has_sent_to_your_mobile_sms,Toast.LENGTH_LONG).show();
                        dialog.setTitle(R.string.label_dialog_title_set_new_password);
                        otp.setVisibility(View.VISIBLE);
                        otp.setEnabled(true);
                        pass1.setVisibility(View.VISIBLE);
                        pass2.setVisibility(View.VISIBLE);
                        pass1.setEnabled(true);
                        pass2.setEnabled(true);
                        otp_btn.setVisibility(View.GONE);
                        confirm_btn.setVisibility(View.VISIBLE);
                        confirm_btn.setEnabled(true);
                        cancel_btn.setVisibility(View.GONE);
                        session = System.currentTimeMillis();
                        forgotOtpDialogId=2;
                        countDownTimer = new OTPCountDownTimer(900000, 1000);
                        countDownTimer.start();
                        dialogBox=1;
                        session = session + 900000;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }




        }
    }




    public class  AddPasswordTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public AddPasswordTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {


            String url = null;
            try {
                url="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/changepassword";
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonParser jParser = new JsonParser();
            response3 = jParser.getJSONFromUrl(url,params[0],"POST",false,null,null);
            return response3;
        }





        @Override
        protected void onPostExecute(String response3) {

            pd.dismiss();


            if(response3.equals("exception"))
            {


                myDialogFragment ob2=new myDialogFragment();


                android.app.FragmentManager fm=getFragmentManager();
                ob2.show(fm, "1");



            }
            else
            {
                try {
                    forgotOtpDialogId=-1;
                    dialogid=-1;
                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("Status");



                    if(msg.equals("Success")) {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Password_successfully_changed, Toast.LENGTH_LONG).show();

                    }
                    else {
                        Toast.makeText(getApplicationContext(),R.string.label_toast_Sorry_please_try_again_later, Toast.LENGTH_SHORT).show();
                       // finish();
                    }
                    dialog.dismiss();
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
            }





        }
    }



    public class  CheckApkVersionTask extends AsyncTask<String, String, String> {

        ProgressDialog pd;
        Context context;

        public CheckApkVersionTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {


            String url = null;
            try {
                url="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/versioncheck?version="+params[0];
            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonParser jParser = new JsonParser();
            response3 = jParser.getJSONFromUrl(url, null, "GET", true, null, null);
            return response3;
        }


        @Override
        protected void onPostExecute(String response3) {

            pd.dismiss();

            if (response3.equals("exception"))
            {
                editor.putInt("versionchk",-1);
                editor.commit();
                initialize();

                         }
            else
            {
                try {

                    JSONObject obj = new JSONObject(response3);
                    String msg = obj.getString("response");


                    if(msg.equals("0"))
                    {
                        editor.putInt("versionchk", 0);
                        editor.commit();

                        initialize();
                    }




                    if(msg.equals("1"))
                    {
                        editor.putInt("versionchk", 1);
                        editor.commit();


                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                        builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        initialize();
                                    }
                                }).setCancelable(false);

                        dialog = builder.show();


                    } else {

                        if(msg.equals("2")) {

                            editor.putInt("versionchk",2);
                            editor.commit();

                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                            builder.setMessage(R.string.label_alertdialog_app_update_msg)
                                    .setPositiveButton(R.string.label_alertdialog_close, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    }).setCancelable(false);

                            dialog = builder.show();


                        }

                    }

                }
                catch(JSONException e){
                    e.printStackTrace();
                }


            }




        }
    }



    public void onHelp(View v)
    {
        Intent intent = new Intent(MainActivity.this,UserManualActivity.class);
        startActivity(intent);

    }


    public void initialize()
    {

        login = sharedPref.getBoolean("login", false);
        if (login) {
            chkversiondialogid=-1;

            Intent i = new Intent(MainActivity.this, DashboardActivity.class);
            startActivity(i);
            finish();
        } else {
            chkversiondialogid=1;

            setContentView(R.layout.activity_main);

            phoneNumber=(EditText)findViewById(R.id.phoneNumber);
            password=(EditText)findViewById(R.id.password);
            chkBox=(CheckBox)findViewById(R.id.sign_chk);

            password.setLongClickable(false);


            if(!sharedPref.getString("mobilenumber","").equals("")&&!sharedPref.getString("password","").equals(""))
            {
                phoneNumber.setText(sharedPref.getString("mobilenumber",""));
                password.setText(sharedPref.getString("password",""));
            }


            phoneNumber.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {


                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after){}
                public void onTextChanged(CharSequence s, int start, int before, int count){

                    password.setError(null);

                }
            });


            phoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {


                    }
                }
            });

            phoneNumber.setLongClickable(false);




            FragmentManager fm = getFragmentManager();
            dataFragment = (RetainedFragment) fm.findFragmentByTag("MainActivity");


            if (dataFragment == null) {
                dataFragment = new RetainedFragment();
                fm.beginTransaction().add(dataFragment, "MainActivity").commit();

                dialogid = -1;

                dataFragment.setdialogId(dialogid);

            } else {

                if (dataFragment.getdialogId() == 1) {
                    System.out.println(dataFragment.getdialogId());
                    ((Button) findViewById(R.id.signUpBtn)).performClick();


                }
                else
                {
                    if (dataFragment.getdialogId() == 2) {
                        System.out.println(dataFragment.getdialogId());
                        number=dataFragment.getMobileNumber();
                        ((Button) findViewById(R.id.forgot)).performClick();
                    }
                }

            }


        }
    }

}