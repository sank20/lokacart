package com.mobile.ict.cart;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.mobile.ict.cart.ProductCategory;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 9/10/15.
 */
public class OrderRowAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<ProductCategory> mGroups;
    private LayoutInflater mInflater;
    OrderRowViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();


    Bundle b;
    HashMap<String,Double> quantities = new HashMap<String, Double>();

    public OrderRowAdapter(Context context, ArrayList<ProductCategory> groups, Bundle bundle) {
        mContext = context;
        mGroups = groups;
        b=bundle;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class OrderRowViewHolder {
        TextView name;
        TextView unitPrice;
        EditText quantity;
        TextView total;
      //  TextView stockStatus;
        int childPos,grpPos;
    }

    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
       // arrTemp = new String[mGroups.size()][mGroups.get(groupPosition).productItems.size()];
        return mGroups.get(groupPosition).productItems.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).productItems.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.category_group, null);
        }

        ProductCategory group = (ProductCategory) getGroup(groupPosition);

        TextView textView = (TextView) convertView.findViewById(R.id.textView1);
        textView.setText(group.categoryName);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(com.mobile.ict.cart.R.layout.order_row,null);
            viewHolder = new OrderRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_name);
            viewHolder.unitPrice = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_unit_rate);
            viewHolder.quantity = (EditText) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_quantity);
            viewHolder.total = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_bill);
          //  viewHolder.stockStatus = (TextView) row.findViewById(R.id.stockstatus);
            row.setTag(viewHolder);
        } else {
            viewHolder = (OrderRowViewHolder)row.getTag();
        }
        OrderRow item = (OrderRow)getChild(groupPosition, childPosition);
        viewHolder.name.setText(item.getName());
        viewHolder.unitPrice.setText(item.getUnitPrice().toString());
        /*if(item.getStockQuantity()==0.0)
        {
         // viewHolder.stockStatus.setText("Out of stock");
            viewHolder.quantity.setEnabled(false);
        }
        else
        {
          //  viewHolder.stockStatus.setText("Stock available");
            viewHolder.quantity.setEnabled(true);
        }*/
        viewHolder.childPos=childPosition;
        viewHolder.grpPos=groupPosition;
        double quantity;

        if(b.containsKey(item.getName())){
            quantity = b.getDouble(item.getName(),0.0);
        }
        else{
            quantity = 0.0;

        }

        viewHolder.quantity.setText(quantity + "");
        item.setTotal(Double.parseDouble(String.format("%.2f", quantity * item.getUnitPrice())));

      //  item.setTotal(quantity*item.getUnitPrice());

        viewHolder.total.setText(item.getTotal().toString());

        final EditText quantity_field = (EditText) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_quantity);
        final TextView total_field = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_bill);
        final TextView rate_field = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_unit_rate);
        final TextView item_name = (TextView) row.findViewById(com.mobile.ict.cart.R.id.new_order_item_name);
       // final TextView stock_status = (TextView) row.findViewById(R.id.stockstatus);

        final double qntstk = item.getStockQuantity();


        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {


                if (!quantity_field.getText().toString().equals("")) {

                    try {
                        Double.parseDouble(quantity_field.getText().toString());

                       /* if(Double.parseDouble(quantity_field.getText().toString())>qntstk)
                            Toast.makeText(mContext,"Insufficient Stock", Toast.LENGTH_SHORT).show();
*/
                        //  else
                        {
                            String str = String.format("%.2f", (Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(quantity_field.getText().toString())));
                            total_field.setText(str + " INR");

                           // total_field.setText(Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(quantity_field.getText().toString()) + " INR");

                            b.putDouble(item_name.getText().toString(), Double.parseDouble(quantity_field.getText().toString()));


                        }



                    } catch (NumberFormatException e) {
                        Toast.makeText(mContext, R.string.label_toast_Please_enter_valid_quantity, Toast.LENGTH_SHORT).show();
                    }
                } else {

                    total_field.setText((Double.parseDouble(rate_field.getText().toString()) * 0.0) + " INR");

                    b.putDouble(item_name.getText().toString(), 0.0);


                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
        });

        return row;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}