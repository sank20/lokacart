package com.mobile.ict.cart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ConfirmOrderActivity extends Activity implements View.OnClickListener{

	Button b1,b2;
	TextView heading;
	private double Total=0.0;
	ArrayList<ItemRow> items;
	String memberNumber,orderid;
	Context context;
	JSONObject order;
	SharedPreferences sharedPref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_order);

		sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		ListView list = (ListView) findViewById(R.id.confirm_order_item_list);
		List<ItemRow> item_list = getItems();
		ItemRowAdapter adapter = new ItemRowAdapter(this,R.layout.view_order_row, item_list);
		list.setAdapter(adapter);
		TextView totalBill = (TextView) findViewById(R.id.confirm_order_total_bill);
		//totalBill.setText(Total+"");
		totalBill.setText(Double.parseDouble(String.format("%.2f",Total))+"");


		b2=(Button)findViewById(R.id.button_checkout);
		b2.setOnClickListener(this);

		b1=(Button)findViewById(R.id.button_modify_order);
		b1.setOnClickListener(this);

		heading=(TextView)findViewById(R.id.tvOrder1);

		String fontPath1 = "fonts/Caviar_Dreams_Bold.ttf";
		Typeface tf1= Typeface.createFromAsset(getAssets(), fontPath1);
		heading.setTypeface(tf1);
		b1.setTypeface(tf1);
		b2.setTypeface(tf1);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_order, menu);
		MenuItem item_help = menu.findItem(R.id.action_help);
		MenuItem item_logout = menu.findItem(R.id.action_logout);
		MenuItem item_home = menu.findItem(R.id.action_home);
		MenuItem item_refresh = menu.findItem(R.id.action_refresh);
		MenuItem item_editNumber = menu.findItem(R.id.action_edit_number);

		item_editNumber.setVisible(false);
		item_editNumber.setEnabled(false);
		item_refresh.setVisible(false);
		item_refresh.setEnabled(false);
		item_home.setVisible(false);
		item_home.setEnabled(false);

		item_help.setVisible(false);
		item_logout.setVisible(false);
		item_help.setEnabled(false);
		item_logout.setEnabled(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case com.mobile.ict.cart.R.id.action_home:
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}

	}
	public List<ItemRow> getItems() {

		items = new ArrayList<>();

		Map<String,String> itemsMap = (Map) getIntent().getSerializableExtra("itemsMap");
		Map<String, String> sortList = new TreeMap<String, String>(itemsMap);
		for(Map.Entry<String, String> entry : sortList.entrySet()){
			double quantity = getIntent().getDoubleExtra(entry.getKey(),0);
			if(quantity > 0) {
				double total = quantity * Double.parseDouble(entry.getValue());

				Total += Double.parseDouble(String.format("%.2f",total));
				//String str = String.format("%.2f",total);

				//Total += Double.parseDouble(str);
				//ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), total);
				ItemRow item = new ItemRow(entry.getKey(), quantity, Double.parseDouble(entry.getValue()), Double.parseDouble(String.format("%.2f",total)));

				items.add(item);
			}
		}


		return items;
	}

	@Override
	public void onClick(View view) {

		if(view==b1){
			Intent i = new Intent(this,NewOrderActivity.class);
			i.putExtras(getIntent().getExtras());
			startActivity(i);
			finishConfirmOrderActivity();
		}
		else if(view==b2){


		    if(Total<=0){
		    	

		    	Toast.makeText(getApplicationContext(),R.string.label_toast_Please_select_at_least_one_product,
		    			   Toast.LENGTH_LONG).show();
		    }
		    else {

			try {
				order = new JSONObject();
				order.put("orgabbr",sharedPref.getString("orgAbbr",""));
				order.put("groupname", "Parent Group");
				JSONArray products = new JSONArray();
				JSONObject object;
				int i=1;
				for(ItemRow item : items)
				{
					object  = new JSONObject();
					object.put("name",item.getName());
					object.put("quantity",item.getQuantity().toString());
					products.put(object);
					i++;
				}
				order.put("orderItems",products);

			} catch (Exception e) {
				e.printStackTrace();
			}


			new WriteToServer(ConfirmOrderActivity.this).execute(order);
			

			}
		    }
		    
	}



	public class WriteToServer extends AsyncTask<JSONObject, String, String> {

		ProgressDialog pd;
		Context context;
		String val;

		public WriteToServer(Context context){
			this.context=context;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			lockScreenOrientation();
			pd = new ProgressDialog(ConfirmOrderActivity.this);
			pd.setMessage(getString(R.string.label_please_wait));
			pd.setCancelable(false);
			pd.show();

		}

		protected String doInBackground(JSONObject... params) {

			String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/api/orders/add";
			JsonParser jParser = new JsonParser();
			String response = jParser.getJSONFromUrl(serverUrl,params[0],"POST",true,sharedPref.getString("emailid",null),sharedPref.getString("password",null));

			return response;

		}

		protected void onPostExecute(String response1) {

			pd.dismiss();


			if(response1.equals("exception"))
			{
				Toast.makeText(context,R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later, Toast.LENGTH_SHORT).show();
			}
			else {
				JSONObject jsonObj = null;
				try {
					jsonObj = new JSONObject(response1);
					val=jsonObj.getString("Status");

					if(val.equals("Success"))
					{

						orderid=jsonObj.getString("orderId");

						Intent i = new Intent(ConfirmOrderActivity.this,OrderSubmitActivity.class);
						i.putExtra("orderid",orderid);
						startActivity(i);
						finishConfirmOrderActivity();

					}

					else

						Toast.makeText(context,R.string.label_toast_Sorry_we_were_unable_to_process_your_request_Please_try_again_later, Toast.LENGTH_SHORT).show();
				} catch (JSONException e) {
					e.printStackTrace();
				}





			}


			unlockScreenOrientation();


		}



		private void lockScreenOrientation() {
			int currentOrientation = getResources().getConfiguration().orientation;
			if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			} else {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		}

		private void unlockScreenOrientation() {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		}

	}
	@Override
	public void onBackPressed() {
		Intent i = new Intent(this,NewOrderActivity.class);
		i.putExtras(getIntent().getExtras());
		startActivity(i);
		finishConfirmOrderActivity();
	}

	public void finishConfirmOrderActivity() {
		ConfirmOrderActivity.this.finish();
	}
}
