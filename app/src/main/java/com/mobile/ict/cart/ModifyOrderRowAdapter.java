package com.mobile.ict.cart;

import android.content.Context;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModifyOrderRowAdapter extends ArrayAdapter<OrderRow>{

    private static final String TAG = "OrderRowArrayAdapter";
    private List<OrderRow> orderItemList = new ArrayList<OrderRow>();
    OrderRowViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    Bundle b;

    HashMap<String,Double> quantities = new HashMap<String, Double>();

    public ModifyOrderRowAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ModifyOrderRowAdapter(Context context, int resource, List<OrderRow> orderItemList, Bundle bundle) {
        super(context, resource, orderItemList);
        this.orderItemList = orderItemList;
        this.context = context;
        this.b = bundle;
    }

    static class OrderRowViewHolder {
        TextView name;
        TextView unitPrice;
        EditText quantity;
        TextView total;
    }

    @Override
    public void add(OrderRow object) {
        orderItemList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderItemList.size();
    }

    @Override
    public OrderRow getItem(int index) {
        return this.orderItemList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.order_row, parent, false);
            viewHolder = new OrderRowViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.new_order_item_name);
            viewHolder.unitPrice = (TextView) row.findViewById(R.id.new_order_item_unit_rate);
            viewHolder.quantity = (EditText) row.findViewById(R.id.new_order_item_quantity);
            viewHolder.total = (TextView) row.findViewById(R.id.new_order_item_bill);
            row.setTag(viewHolder);
        } else {
            viewHolder = (OrderRowViewHolder)row.getTag();
        }
        OrderRow item = getItem(position);
        viewHolder.name.setText(item.getName());
        viewHolder.unitPrice.setText(item.getUnitPrice().toString());

        double quantity;
        if(qmap.containsKey(item.getName())){
            quantity = qmap.get(item.getName());
        }
        else if(b.containsKey(item.getName())){
            quantity = b.getDouble(item.getName(),0.0);
        }
        else{
            quantity = 0;
            qmap.put(item.getName(),0.0);
        }

        viewHolder.quantity.setText(quantity + "");
        item.setTotal(Double.parseDouble(String.format("%.2f", quantity * item.getUnitPrice())));

       // item.setTotal(quantity*item.getUnitPrice());
        viewHolder.total.setText(item.getTotal().toString());

        final EditText quantity_field = (EditText) row.findViewById(R.id.new_order_item_quantity);
        final TextView total_field = (TextView) row.findViewById(R.id.new_order_item_bill);
        final TextView rate_field = (TextView) row.findViewById(R.id.new_order_item_unit_rate);
        final TextView item_name = (TextView) row.findViewById(R.id.new_order_item_name);



        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {


                if (!quantity_field.getText().toString().equals("")) {

                    try {
                        Double.parseDouble(quantity_field.getText().toString());
                       // total_field.setText(Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(quantity_field.getText().toString()) + " INR");

                        String str = String.format("%.2f", (Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(quantity_field.getText().toString())));
                        total_field.setText(str + " INR");
                        b.putDouble(item_name.getText().toString(), Double.parseDouble(quantity_field.getText().toString()));

                    } catch (NumberFormatException e) {
                        Toast.makeText(context, R.string.label_toast_Please_enter_valid_quantity, Toast.LENGTH_SHORT).show();
                    }
                } else {

                    total_field.setText((Double.parseDouble(rate_field.getText().toString()) * 0.0) + " INR");

                    b.putDouble(item_name.getText().toString(), 0.0);


                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
        });


       /*
        quantity_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if(s.length() != 0 )
                {

                    if(s.charAt(0)=='.')s="0"+s.toString();


                      total_field.setText(Double.parseDouble(rate_field.getText().toString()) * Double.parseDouble(s.toString()) + " INR");


                     b.putDouble(item_name.getText().toString(), Double.parseDouble(s.toString()));
                      qmap.put(item_name.getText().toString(), Double.parseDouble(s.toString()));



                }
                else
                {

                    total_field.setText((Double.parseDouble(rate_field.getText().toString()) * 0.0) + " INR");

                    b.putDouble(item_name.getText().toString(),0.0);
                    qmap.put(item_name.getText().toString(),0.0);





                }
            }
        });


        ((EditText)row.findViewById(R.id.new_order_item_quantity)).setOnEditorActionListener(
                new EditText.OnEditorActionListener() {


                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (
                                actionId == EditorInfo.IME_ACTION_DONE

                                       ) {
                            if (quantity_field.getText().toString().equals("")) {
                                quantity_field.setText("0.0");
                                return true;
                            }
                            if (quantity_field.getText().toString().charAt(0)=='.') {
                                quantity_field.setText("0"+quantity_field.getText().toString());
                                return true;
                            }
                        }
                        return false;
                    }
                });

        */

        return row;
    }


}